# README #

### Bearstrike OpenRail by Bearocratic Designs ###
https://fb.me/BearocraticDesigns
http://shop.bearocratic.com/


### What is this repository for? ###
The OpenRail system is a modular rail/lens setup for (but not limited to) laser skirmish taggers.
The idea is the you pick and choose the parts you need then print. If a part is not right for you then you can modify and existing section so for example if you are wanting to use a different lens then you can create the part you need using an existing part as a base. 


After you have printed all the parts you need then mount to your tagger. 


This is designed to go with the Bearocratic Designs Bearstrike Lasertag system but can be used for other lasertag systems like Fragtag or MilesTag. 

The initial base parts are minimum parts required for building a lens attachment for a Nerf tagger using a 42mm achromatic lens.
(Lens can be purchased from here http://shop.bearocratic.com/lenses-optics/47-lens-42mm-achromatic-bk7-uncoated.html )   
![Bearstrike_OpenRail_parts.jpg](https://bitbucket.org/repo/BjzLrL/images/2639285191-Bearstrike_OpenRail_parts.jpg)


In the example above, 
The Orange part is the Nerf mount for mounting to a Nerf blaster
The grey part is the 42mm Achromatic lens mount.
The White part is the 42mm lens cap.
The Purple part is the midsection (print multiple of these).



You will need to print multiple mid sections to make up the focal length of the lens (at least 5) then a lens mount and end. If you are mounting to a Nerf Tagger then you will also need to print the mounting part too. After printing all the parts it is recommended to support them all with a long metal bolt.  
(Pro tip: You can join multiple part together before printing)
![Bearstrike_OpenRail_example.jpg](https://bitbucket.org/repo/BjzLrL/images/816404931-Bearstrike_OpenRail_example.jpg)



#Modifying of parts#
The parts were created with Tinkercad to make as easy as possible for people to modify or just tinker with the files. 
The source files can be accessed from this link https://www.tinkercad.com/things/hoHpKZUHTQs-bearstrike-openrail
Please use the tag OpenRail if you share you version so others can find them.



#Legal Note#

Please check your local laws before printing. 

If in Victoria imitation firearms (anything that can be resemble mistaken as a firearm) requires a permit. So it is recommended you print in a "color that is not commonly associated with a working firearm".  


 
#License#
This project is released under the following license. 
Attribution-NonCommercial-ShareAlike 
CC BY-NC-SA

This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.